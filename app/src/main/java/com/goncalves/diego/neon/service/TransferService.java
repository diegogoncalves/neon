package com.goncalves.diego.neon.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.goncalves.diego.neon.Utils.Util;
import com.goncalves.diego.neon.models.Person;
import com.goncalves.diego.neon.models.response.GetTransfersResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diego on 8/30/2017.
 */

public class TransferService extends AsyncTask<String, JSONObject, List<GetTransfersResponse>>
{
    private static String BaseURL = "http://processoseletivoneon.azurewebsites.net/";
    private Context Context;

    public TransferService(Context _context)
    {
        this.Context = _context;
    }

    @Override
    protected List<GetTransfersResponse> doInBackground(String... strings)
    {
        return this.GetTransfers();
    }

    public List<GetTransfersResponse> GetTransfers()
    {
        String token = Util.getInstance().readPreference(this.Context, "TOKEN", "");

        try
        {
            HttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(BaseURL + "/GetTransfers?token="+ token);

            HttpResponse response = client.execute(httpGet);

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;

            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuilder.append(line);
            }

            bufferedReader.close();

            JSONArray array = new JSONArray(stringBuilder.toString());

            List<JSONObject> objs = new ArrayList<JSONObject>();

            for (int i = 0; i < array.length(); i++)
            {
                objs.add(array.getJSONObject(i));
            }

            return this.ConvertTransfers(objs);
        }
        catch(Exception ex)
        {
            Log.e("Error", ex.getMessage());
        }

        return null;
    }

    private List<GetTransfersResponse> ConvertTransfers(List<JSONObject> objs) throws JSONException
    {
        List<GetTransfersResponse> transfers = new ArrayList<GetTransfersResponse>();

        List<Person> people = PeopleService.getInstance(Context).getPeopleList();

        for (int i = 0; i < objs.size(); i++)
        {
            GetTransfersResponse response = new GetTransfersResponse();

            response.setClientId(objs.get(i).getInt("ClienteId"));
            response.setId(objs.get(i).getInt("Id"));
            response.setData(objs.get(i).getString("Data"));
            response.setToken(objs.get(i).getString("Token"));
            response.setValor(objs.get(i).getDouble("Valor"));

            for (Person p : people)
            {
                if(response.getClientId().equals(p.getId()))
                {
                    response.setName(p.getName());
                    response.setPhone(p.getPhone());
                }
            }

            transfers.add(response);
        }

        return transfers;
    }
}
