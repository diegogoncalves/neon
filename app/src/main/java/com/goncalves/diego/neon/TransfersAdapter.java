package com.goncalves.diego.neon;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.goncalves.diego.neon.Utils.Util;
import com.goncalves.diego.neon.models.response.GetTransfersResponse;

import java.util.List;

/**
 * Created by Diego on 9/15/2017.
 */

public class TransfersAdapter extends ArrayAdapter<GetTransfersResponse>
{
    private Context _context;
    private List<GetTransfersResponse> _transfers;

    public TransfersAdapter(@NonNull Context context, List<GetTransfersResponse> objects)
    {
        super(context, R.layout.transfers_row, objects);

        this._context = context;
        this._transfers = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        TransferViewHolder viewHolder;

        if(convertView == null)
        {
            LayoutInflater inflater = ((Activity) _context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.transfers_row, parent, false);

            viewHolder = new TransferViewHolder();

            viewHolder.TransferContactName = (TextView) convertView.findViewById(R.id.transfers_row_name);
            viewHolder.TransferContactPhone = (TextView) convertView.findViewById(R.id.transfers_row_phone);
            viewHolder.TransferValue = (TextView) convertView.findViewById(R.id.transfers_row_value);

            convertView.setTag(viewHolder);
        }
        else
            viewHolder = (TransferViewHolder) convertView.getTag();

        viewHolder.TransferContactName.setText(getItem(position).getName());
        viewHolder.TransferContactPhone.setText(getItem(position).getPhone());
        viewHolder.TransferValue.setText(Util.getInstance().getNumberFormatPortuguese().format(getItem(position).getValor()));

        return convertView;
    }

    static class TransferViewHolder
    {
        TextView TransferContactName;
        TextView TransferContactPhone;
        TextView TransferValue;
    }
}
