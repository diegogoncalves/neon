package com.goncalves.diego.neon.service;

import android.content.Context;
import android.util.Log;

import com.goncalves.diego.neon.Utils.Util;
import com.goncalves.diego.neon.models.Person;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diego on 8/30/2017.
 */

public class PeopleService
{
    private static PeopleService instance = null;
    private static Context _context;

    protected PeopleService()
    {}

    public static PeopleService getInstance(Context context)
    {
        _context = context;

        if(instance == null)
            instance = new PeopleService();

        return instance;
    }

    public List<Person> getPeopleList()
    {
        List<Person> people = new ArrayList<Person>();

        try
        {
            String json = Util.getInstance().readPreference(this._context, "PEOPLE", "");

            JSONArray jsonArray = new JSONArray(json);
            List<JSONObject> objs = new ArrayList<JSONObject>();

            for (int i = 0; i < jsonArray.length(); i++)
            {
                objs.add(jsonArray.getJSONObject(i));
            }

            for (int i = 0; i < objs.size(); i++)
            {
                Person person = new Person();

                person.setId(objs.get(i).getInt("id"));
                person.setName(objs.get(i).getString("name"));
                person.setPhone(objs.get(i).getString("phone"));

                people.add(person);
            }
        }
        catch(Exception ex)
        {
            Log.e("getPeopleList error =", ex.getMessage());
        }
        finally
        {
            return people;
        }
    }
}
