package com.goncalves.diego.neon.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.goncalves.diego.neon.Utils.Util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

/**
 * Created by Diego on 8/30/2017.
 */

public class TokenService extends AsyncTask<String, JSONObject, String>
{
    private static String BaseURL = "http://processoseletivoneon.azurewebsites.net/";
    private Context Context;

    public TokenService(Context _context)
    {
        this.Context = _context;
    }

    @Override
    protected String doInBackground(String... strings)
    {
        String token = this.GenerateToken();

        token = token.replaceAll("\"", "");

        Util.getInstance().savePreferences(this.Context, "TOKEN", token);

        return token;
    }

    public String GenerateToken()
    {
        String Json = null;

        try
        {
            HttpClient client = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet();
            httpGet.setURI(new URI(BaseURL + "GenerateToken?nome=diego&email=diego.goncalves@outlook.com"));

            HttpResponse response = client.execute(httpGet);

            HttpEntity httpEntity = response.getEntity();

            if(httpEntity != null)
            {
                InputStream inputStream = httpEntity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null)
                {
                    stringBuilder.append(line);
                }

                inputStream.close();
                Json = stringBuilder.toString();
            }
        }
        catch (Exception ex)
        {
            Log.e("", ex.getMessage().toString());
        }

        return Json;
    }
}
