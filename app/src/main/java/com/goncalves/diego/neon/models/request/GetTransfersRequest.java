package com.goncalves.diego.neon.models.request;

/**
 * Created by Diego on 8/23/2017.
 */
public class GetTransfersRequest
{
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
