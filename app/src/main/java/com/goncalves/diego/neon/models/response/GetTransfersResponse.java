package com.goncalves.diego.neon.models.response;

/**
 * Created by Diego on 8/23/2017.
 */
public class GetTransfersResponse
{
    private Integer Id;
    private Integer ClientId;
    private Double Valor;
    private String Token;
    private String Data;
    private String Name;
    private String Phone;

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public Double getValor() {
        return Valor;
    }

    public void setValor(Double valor) {
        Valor = valor;
    }

    public Integer getClientId() {
        return ClientId;
    }

    public void setClientId(Integer clientId) {
        ClientId = clientId;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }
}
