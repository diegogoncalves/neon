package com.goncalves.diego.neon.models.request;

/**
 * Created by Diego on 8/23/2017.
 */
public class GenTokenRequest
{
    private String _nome;

    public String getNome() {
        return _nome;
    }

    public void setNome(String nome) {
        this._nome = nome;
    }

    private String _email;

    public String getEmail() {
        return _email;
    }

    public void setEmail(String email) {
        this._email = email;
    }


}
