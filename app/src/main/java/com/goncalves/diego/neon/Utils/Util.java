package com.goncalves.diego.neon.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Diego on 8/29/2017.
 */

public class Util
{
    private static Util instance = null;

    protected Util()
    {}

    public static Util getInstance()
    {
        if(instance == null)
            instance = new Util();

        return instance;
    }

    public void savePreferences(Context context, String key, String value)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("TESTE", context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(key, value);
        editor.commit();
    }

    public String readPreference(Context context, String key, String value)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("TESTE", context.MODE_PRIVATE);

        return sharedPreferences.getString(key, value);
    }

    public NumberFormat getNumberFormatPortuguese()
    {
        return NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
    }
}
