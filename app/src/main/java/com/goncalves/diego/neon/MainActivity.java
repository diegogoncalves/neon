package com.goncalves.diego.neon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.goncalves.diego.neon.service.TokenService;

public class MainActivity extends Activity
{
    private Button btnSendMoney, btnTransfers;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TokenService tokenService = new TokenService(getApplicationContext());
        tokenService.execute();

        this.btnSendMoney = (Button)findViewById(R.id.btnSendMoney);
        this.btnTransfers = (Button)findViewById(R.id.btnTransfers);

        this.btnSendMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SendMoneyContactActivity.class);
                startActivity(intent);
            }
        });

        this.btnTransfers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TransfersActivity.class);
                startActivity(intent);
            }
        });
    }
}
