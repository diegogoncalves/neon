package com.goncalves.diego.neon.controls;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Diego on 8/31/2017.
 */

public class MoneyEditText implements TextWatcher {

    private final EditText mText;
    private boolean isUpdating;

    @SuppressWarnings("static-access")
    public MoneyEditText(EditText mText) {
        this.mText = mText;
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (isUpdating) {
            isUpdating = false;
            return;
        }
        isUpdating = true;
        String str = s.toString();
        str = str.replaceAll("[^\\d]", "");

        try
        {
            str = NumberFormat.getCurrencyInstance(new Locale("pt","BR")).format(Double.parseDouble(str) / 100);
            str = str.replace("R$", "");
            str = str.replace("$", "");

            this.mText.setText(str);
            this.mText.setSelection(this.mText.getText().length());
        }
        catch(NumberFormatException e) {
            s = "";
        }
    }
}
