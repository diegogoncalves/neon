package com.goncalves.diego.neon;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;

import com.goncalves.diego.neon.models.response.GetTransfersResponse;
import com.goncalves.diego.neon.service.TransferService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diego on 8/31/2017.
 */

public class TransfersActivity extends ActionBarActivity
{
    private ListView _list;
    private android.support.v7.widget.Toolbar toolbar;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.transfers_activity);

        this._list = (ListView)findViewById(R.id.transfers_activity_list);
        this.toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TransfersAdapter listAdapter = new TransfersAdapter(this, this.getTransfersList());
        this._list.setAdapter(listAdapter);
    }

    private List<GetTransfersResponse> getTransfersList()
    {
        List<GetTransfersResponse> response = new ArrayList<GetTransfersResponse>();

        try
        {
            TransferService transferService = new TransferService(getApplicationContext());
            response = transferService.execute().get();
        }
        catch (Exception ex)
        {
            Log.e("getTransferList error", ex.getMessage());
        }
        finally
        {
            return response;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }
        else
        {
            this.finish();
        }


        return super.onOptionsItemSelected(item);
    }
}
