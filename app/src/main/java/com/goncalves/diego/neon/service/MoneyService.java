package com.goncalves.diego.neon.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.goncalves.diego.neon.Utils.Util;
import com.goncalves.diego.neon.models.request.SendMoneyRequest;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diego on 8/30/2017.
 */

public class MoneyService extends AsyncTask<String, JSONObject, Boolean>
{
    private static String BaseURL = "http://processoseletivoneon.azurewebsites.net/";
    private android.content.Context Context;

    public MoneyService(Context _context)
    {
        this.Context = _context;
    }

    @Override
    protected Boolean doInBackground(String... strings)
    {
        return this.SendMoney(strings[0], Double.parseDouble(strings[1]));
    }

    public Boolean SendMoney(String clientId, Double value)
    {
        String token = Util.getInstance().readPreference(this.Context, "TOKEN", "");
        String result = null;

        SendMoneyRequest request = new SendMoneyRequest();
        request.setClientId(clientId);
        request.setToken(token);
        request.setValor(value);

        try
        {
            HttpClient client = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(BaseURL + "/SendMoney");

            List<NameValuePair> parameters = new ArrayList<NameValuePair>();
            parameters.add(new BasicNameValuePair("ClienteId", request.getClientId()));
            parameters.add(new BasicNameValuePair("token", request.getToken()));
            parameters.add(new BasicNameValuePair("valor", request.getValor().toString()));

            httpPost.setEntity(new UrlEncodedFormEntity(parameters));

            HttpResponse response = client.execute(httpPost);

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;

            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuilder.append(line);
            }

            bufferedReader.close();

            result = stringBuilder.toString();

            Log.d("Send Money result =", result);

        }
        catch (Exception ex)
        {
            Log.e("Error Send Money", ex.getMessage());
        }

        return Boolean.parseBoolean(result);
    }
}
