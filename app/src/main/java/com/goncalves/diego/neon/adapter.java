package com.goncalves.diego.neon;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.goncalves.diego.neon.models.Person;

import java.util.List;

/**
 * Created by Diego on 8/31/2017.
 */

public class adapter extends ArrayAdapter<Person>
{
    private Context _context;
    private List<Person> _people;

    public adapter(@NonNull Context context, List<Person> objects) {
        super(context, R.layout.send_money_contact_row, objects);

        this._context = context;
        this._people = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;

        if(convertView == null)
        {
            LayoutInflater inflater = ((Activity) _context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.send_money_contact_row, parent, false);

            holder = new ViewHolder();

            holder.sendMoneyContactName = (TextView) convertView.findViewById(R.id.send_money_contact_row_name);
            holder.sendMoneyContactPhone = (TextView) convertView.findViewById(R.id.send_money_contact_row_phone);

            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.sendMoneyContactName.setText(getItem(position).getName());
        holder.sendMoneyContactPhone.setText(getItem(position).getPhone());

        return convertView;
    }

    static class ViewHolder{
        TextView sendMoneyContactName;
        TextView sendMoneyContactPhone;
    }
}


