package com.goncalves.diego.neon;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.goncalves.diego.neon.components.AlertDialog;
import com.goncalves.diego.neon.models.Person;
import com.goncalves.diego.neon.service.PeopleService;

/**
 * Created by Diego on 9/1/2017.
 */

public class SendMoneyContactActivity extends ActionBarActivity
{
    private ListView _list;
    private android.support.v7.widget.Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.send_money_contact);

        this._list = (ListView) findViewById(R.id.send_money_contact_list);
        this.toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter listAdapter = new adapter(this, PeopleService.getInstance(this).getPeopleList());
        this._list.setAdapter(listAdapter);

        this._list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                final Person person = (Person) _list.getAdapter().getItem(position);
                AlertDialog.showDialog(SendMoneyContactActivity.this, person.getName(), person.getPhone(), String.valueOf(person.getId()));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }
        else
        {
            this.finish();
        }


        return super.onOptionsItemSelected(item);
    }
}
