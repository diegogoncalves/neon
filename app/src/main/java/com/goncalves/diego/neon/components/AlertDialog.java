package com.goncalves.diego.neon.components;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.goncalves.diego.neon.R;
import com.goncalves.diego.neon.controls.MoneyEditText;
import com.goncalves.diego.neon.service.MoneyService;

/**
 * Created by Diego on 8/30/2017.
 */

public class AlertDialog extends android.support.v7.app.AlertDialog
{
    protected AlertDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(android.os.Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    public static void showDialog(final Context context, String userName, String userPhone, final String userId)
    {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.custom_dialog, null, false);

        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        TextView custom_dialog_user_name = (TextView) dialog.findViewById(R.id.custom_dialog_user_name);
        custom_dialog_user_name.setText(userName);

        TextView custom_dialog_user_phone = (TextView) dialog.findViewById(R.id.custom_dialog_user_phone);
        custom_dialog_user_phone.setText(userPhone);

        Button custom_dialog_confirm = (Button) dialog.findViewById(R.id.custom_dialog_confirm);
        final EditText custom_dialog_transfer_value = (EditText) dialog.findViewById(R.id.custom_dialog_transfer_value);

        custom_dialog_transfer_value.addTextChangedListener(new MoneyEditText(custom_dialog_transfer_value));
        custom_dialog_transfer_value.setInputType(InputType.TYPE_CLASS_NUMBER);

        custom_dialog_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String transferValue = custom_dialog_transfer_value.getText().toString().trim().replace(".","").replace(",",".");

                MoneyService moneyService = new MoneyService(context);
                moneyService.execute(userId, transferValue);

                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
