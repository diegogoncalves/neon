package com.goncalves.diego.neon.models.request;

/**
 * Created by Diego on 8/23/2017.
 */
public class SendMoneyRequest
{
    private String ClientId;
    private String token;
    private Double valor;

    public String getClientId() {
        return ClientId;
    }

    public void setClientId(String clientId) {
        ClientId = clientId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
