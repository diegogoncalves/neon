package com.goncalves.diego.neon.models.response;

/**
 * Created by Diego on 8/23/2017.
 */
public class GenTokenResponse
{
    private String authToken;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
